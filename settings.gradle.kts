pluginManagement {
    repositories {
        jcenter()
        mavenCentral()
        gradlePluginPortal()
    }
}

rootProject.name = "kanon.konfig"

enableFeaturePreview("STABLE_PUBLISHING")