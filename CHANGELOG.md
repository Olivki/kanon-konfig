## 1.1.0 (2019-03-17)

- {---} Removed the `CREATE_NEW` enum constant from `moe.kanon.konfig.settings.UnknownEntryBehaviour`

- {+++} Added the `moe.kanon.konfig.settings.XmlRootNamePlacement` enum, and the appropriate property to `moe.kanon.konfig.settings.KonfigSettings`

## 1.0.0 (2019-03-17)

Initial release